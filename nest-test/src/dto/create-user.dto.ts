export class CreateUserDto {
  user_id: string;
  user_name: string;
  user_email: string;
}
