import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import config from './config/conf'


async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const logger = new Logger(bootstrap.name);
  await app.listen(config.app.PORT);
  logger.log(`App started on port ${config.app.PORT}`)
}
bootstrap();
