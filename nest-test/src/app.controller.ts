import { Controller, Get, Logger, Req } from '@nestjs/common';
import { AppService } from './app.service';
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  private logger: Logger = new Logger(AppController.name); // Create a NestJS logger for AppController

  @Get()
  getHello(): string {
    this.logger.log("Get request");
    return this.appService.getHello();
  }
}
