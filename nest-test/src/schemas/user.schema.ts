import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";


export type UserDocument = User & Document;

@Schema()
export class User {
    @Prop({required: true})
    user_id: string;

    @Prop({required: true})
    user_name: string;

    @Prop({required: true})
    user_email: string;
}

export const UserSchema = SchemaFactory.createForClass(User);