const config = {
  app: {
    PORT: 3000,
  },
  database: {
    IP: '192.168.51.102',
    PORT: 27017,
  },
};

export default config;
