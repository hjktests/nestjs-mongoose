import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { from } from 'rxjs';
import { User, UserDocument } from 'src/schemas/user.schema';
import {CreateUserDto} from '../dto/create-user.dto'

@Injectable()
export class UserService {
    constructor(@InjectModel(User.name) private userModel: Model<UserDocument>){}

    // This creates a new user with userModel and received request body and returns it
    async create(createUserDto: CreateUserDto): Promise<User>{
        const createdUser = new this.userModel(createUserDto);
        return createdUser.save();
    }

    // This returns all users from database
    async findAll():Promise<User[]>{
        return this.userModel.find().exec();
    }

    // This returns user with correct id from database
    async findWithId(id: string):Promise<User>{
        // return this.userModel.find({user_id: "postman-test"}).exec();
        return this.userModel.findOne({user_id: id}).exec();
    }
}
