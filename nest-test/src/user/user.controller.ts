import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { CreateUserDto } from 'src/dto/create-user.dto';
import { User } from 'src/schemas/user.schema';
import { UserService } from './user.service';

@Controller('users')
export class UserController {
    constructor(private userService: UserService){}

    // get all users from /
    @Get()
    async getAll(): Promise<User[]>{
        return this.userService.findAll();
    }

    // get one user with id from /:id
    @Get(':id')
    async getWithId(@Param() params): Promise<User>{
        return this.userService.findWithId(params.id);
    }

    // create user with post to /
    @Post()
    async create(@Body() createUserDto: CreateUserDto){
        console.log(createUserDto);
        return this.userService.create(createUserDto);
    }
}
